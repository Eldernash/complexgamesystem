﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RPGSystem {
    public class Backpack : MonoBehaviour, IDragAndDropContainer {

        public Character character;
        public bool isBelt;
        public int rows, columns;
        public GameObject prefab;
        public float spacing;
        public Vector2 origin;
        List<Item> items = new List<Item>();

        public Item.ItemType itemType = Item.ItemType.ALL;
        public Text description;
        
        // To begin dragging, it must be a item with a valid item
        public bool CanDrag(Draggable obj) {
            ItemInstance item = obj as ItemInstance;
            if (!item || !item.item) return false;
            return true;
        }

        // To be able to drop, it must be a item of the appropriate type
        public virtual bool CanDrop(Draggable dragged, Draggable drop) {
            ItemInstance item = dragged as ItemInstance;

            // Must be a item
            if (item == null) return false;

            // Replacing empty slots in here is OK
            if (item.item == null) return true;

            // check the type of items in the belt vs what we can hold
            int mask = (int)item.item.itemType & (int)itemType;
            return mask != 0;
        }

        public void Drop(Draggable dragged, Draggable replaced, int replacedIndex) {
            // Get the indexes of the drop position in us, and set accordingly
            items[replacedIndex] = (dragged as ItemInstance).item;
            dragged.index = replacedIndex;
        }

        void UpdateInventory() {
            ItemInstance[] cItems;

            cItems = GetComponentsInChildren<ItemInstance>();

            // Deletes all charmInstances for updating
            for (int i = 0; i < cItems.Length; i++) {
                Destroy(cItems[i].GetComponent<Image>());
                Destroy(cItems[i].gameObject);
            }

            // Create a CharmInstance for each item in the list
            int index = 0;
            for (int j = 0; j < rows; j++) {
                for (int i = 0; i < columns; i++) {
                    GameObject go = Instantiate(prefab, transform);
                    go.transform.localPosition = IndexToPosition(i, j);
                    if (index < items.Count) {
                        go.GetComponent<ItemInstance>().index = index;
                        go.GetComponent<ItemInstance>().item = items[index];
                        go.gameObject.SetActive(this.GetComponent<Image>().enabled);
                        index++;
                    }
                }
            }
        }

        private List<Item> itemTest;
        void Update() {
            for (int i = 0; i < character.inventory.Count; i++) {
                if (itemTest[i] != items[i]) {
                    UpdateInventory();
                    itemTest[i] = items[i];
                }
            }
        }

        Vector3 IndexToPosition(int i, int j) {
            return new Vector3(origin.x + spacing * i, origin.y - spacing * j, 0);
        }

        // Use this for initialization
        void Start() {
            // hook up the appropriate array
            items = character.inventory;
            itemTest = new List<Item>(new Item[items.Count]);

            // Create a CharmInstance for each item in the list
            int index = 0;
            for (int j = 0; j < rows; j++) {
                for (int i = 0; i < columns; i++) {
                    GameObject go = Instantiate(prefab, transform);
                    go.transform.localPosition = IndexToPosition(i, j);
                    if (index < items.Count) {
                        go.GetComponent<ItemInstance>().index = index;
                        go.GetComponent<ItemInstance>().item = items[index];
                        index++;
                    }
                }
            }
        }

        public void Discard(Draggable dragged) {

            // backpacks only hold Items, so we know this cast is safe
            ItemInstance itemInst = dragged as ItemInstance;
            
            itemInst.item.Discard(character.transform);
            character.inventory[dragged.index] = null;
        }
    }
}