﻿using System;
using UnityEngine;
namespace RPGSystem {
    [CreateAssetMenu(fileName = "New Item", menuName = "Item")]
    public class Item : ScriptableObject {
        public enum ItemType {
            POTION = 1,
            HELMET = 2,
            ARMOUR = 4,
            RING = 8,
            AMULET = 16,
            BOOTS = 32,
            GLOVES = 64,
            ALL = 127 // combination of all types
        }

        [Tooltip("The item spawned when the item is dropped")]
        public GameObject entityPrefab;

        [Tooltip("What slot the item can be equipped in")]
        public ItemType itemType = ItemType.POTION;

        [Tooltip("The image the item will appear as")]
        public Sprite icon;

        [Tooltip("The colour of the image")]
        public Color color = new Vector4(1,1,1,1);


        // Add any status effects here you want to apply to the character
        [Tooltip("The status effects that will be applied to the character")]
        public StatusEffect[] statEffects;
        
        public void Apply(Character character) {
            foreach (StatusEffect se in statEffects) {
                if (se != null) {
                    se.itemEffect = true;
                    se.Apply(character);
                }
            }
        }
        public void Discard(Transform spawnPos) {
            Instantiate(entityPrefab, spawnPos.position, new Quaternion(0, 0, 0, 0));
        }
    }
}
