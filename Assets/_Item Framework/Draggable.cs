﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace RPGSystem {
    public class Draggable : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {
        public int index;

        Vector3 originalPosition;
        bool dragging;
        IDragAndDropContainer containerDrag;

        const string DRAGGABLE_TAG = "UIDraggable";

        public void OnBeginDrag(PointerEventData eventData) {
            containerDrag = GetContainer();

            dragging = containerDrag == null || containerDrag.CanDrag(this);

            if (dragging) {
                // save our start position
                originalPosition = transform.position;
                // move this and parent to the very front of the UI, so the dragged element draws over everything
                transform.SetAsLastSibling();
                transform.parent.SetAsLastSibling();
                // make it raycast-transparent so we can raycast underneath when we drop
                GetComponent<Image>().raycastTarget = false;
            }
        }

        public void OnDrag(PointerEventData eventData) {
            // move the dragged object with the mouse
            if (dragging)
                transform.position = eventData.position;
        }

        public void OnEndDrag(PointerEventData eventData) {
            if (!dragging)
                return;

            // raycast to find what we're dropping over
            Draggable target = GetDraggableUnderMouse();

            // make the dropped object visible to raycasts again
            GetComponent<Image>().raycastTarget = true;

            IDragAndDropContainer containerTarget = null;

            // check that this move is OK with both containers
            if (target) {
                containerTarget = target.GetContainer();

                // if there is a container and we can't drop into it for game logic reasons, cancel the drag
                if (containerTarget != null)
                    if (containerTarget.CanDrop(this, target) == false)
                        target = null;

                // check the other way too, since the drag and drop is a swap
                if (containerDrag != null)
                    if (containerDrag.CanDrop(target, this) == false)
                        target = null;
            }

            // if we have a valid drop target, drop away!
            if (target) {
                // swap positions
                transform.position = target.transform.position;
                target.transform.position = originalPosition;

                // store the indexes of where these objects came from to pass through, they may get changed in Drop() calls
                int dragIndex = index;
                int replaceIndex = target.index;

                // game logic - let both containers know about the update
                if (containerTarget != null)
                    containerTarget.Drop(this, target, replaceIndex);
                if (containerDrag != null)
                    containerDrag.Drop(target, this, dragIndex);

                // swap parents
                Transform p = transform.parent;
                transform.SetParent(target.transform.parent);
                target.transform.SetParent(p);
            } else {
                // snap back to original position if we dropped over empty space or an invalid target
                transform.position = originalPosition;

                containerDrag.Discard(this);
            }
            dragging = false;
        }

        protected void Init() {
            tag = DRAGGABLE_TAG;
        }

        public IDragAndDropContainer GetContainer() {
            return transform.parent.GetComponent<IDragAndDropContainer>();
        }

        // finds the UI object currently under the mouse
        private GameObject GetObjectUnderMouse() {
            List<RaycastResult> hitObjects = new List<RaycastResult>();
            PointerEventData pointer = new PointerEventData(EventSystem.current);
            pointer.position = Input.mousePosition;
            EventSystem.current.RaycastAll(pointer, hitObjects);
            return (hitObjects.Count <= 0) ? null : hitObjects[0].gameObject;
        }

        // get the object under the mouse as a Draggable
        private Draggable GetDraggableUnderMouse() {
            GameObject go = GetObjectUnderMouse();

            // get the Draggable component
            if (go != null)
                return go.GetComponent<Draggable>();

            return null;
        }
    }
}