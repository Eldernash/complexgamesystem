﻿namespace RPGSystem {
    public interface IDragAndDropContainer {
        bool CanDrag(Draggable obj);
        bool CanDrop(Draggable dragged, Draggable drop);
        void Drop(Draggable dragged, Draggable drop, int replacedIndex);
        void Discard(Draggable dragged);
    }
}