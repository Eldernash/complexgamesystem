﻿using UnityEngine;
using UnityEngine.UI;

namespace RPGSystem {
    public class EquipScreen : MonoBehaviour, IDragAndDropContainer {
        public Character character;

        // we can drag any item that's equipped
        public bool CanDrag(Draggable obj) {
            ItemInstance item = obj as ItemInstance;
            return item && item.item;
        }

        // we can drop anything in the right slot
        public bool CanDrop(Draggable dragged, Draggable drop) {
            ItemInstance item = dragged as ItemInstance;

            // if its not a ItemInstance, not here.
            if (item == null)
                return false;

            // if its a item, make sure its the right type
            if (item.item) {
                int mask = (int)item.item.itemType & 255;
                return mask == (drop.index & 255);
            }

            // if we're dragging an empty slot, so that's OK then.
            return true;
        }

        public void Drop(Draggable dragged, Draggable drop, int replacedIndex) {
            dragged.index = replacedIndex;
            ItemInstance it = dragged as ItemInstance;
            ReplaceItem(replacedIndex, it.item);
            character.CheckItems();
        }

        public void Discard(Draggable dragged) {
            ItemInstance itemInst = dragged as ItemInstance;
            itemInst.item.Discard(character.transform);
            ReplaceItem(dragged.index, null);

            // changes the item's icon back to the default
            itemInst.item = null;

            Image[] childSprite = itemInst.GetComponentsInChildren<Image>();
            foreach (Image ch in childSprite) {
                ch.sprite = itemInst.emptyItemImage;
            }
            itemInst.GetComponent<Image>().sprite = itemInst.backdropImage;

            character.CheckItems();
        }

        // replaces the item at the selected index with the new item
        void ReplaceItem(int index, Item it) {
            switch (index) {
                case (int)Item.ItemType.AMULET: character.amulet = it; break;
                case (int)Item.ItemType.ARMOUR: character.armour = it; break;
                case (int)Item.ItemType.BOOTS: character.boots = it; break;
                case (int)Item.ItemType.GLOVES: character.gloves = it; break;
                case (int)Item.ItemType.HELMET: character.helmet = it; break;
                case (int)Item.ItemType.RING: character.ring1 = it; break;
                case (int)Item.ItemType.RING + 256: character.ring2 = it; break;
            }
        }
    }
}
