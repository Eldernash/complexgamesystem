﻿using UnityEngine;
using UnityEngine.UI;

namespace RPGSystem {
    public class ItemInstance : Draggable {
        [Tooltip("An image border that displays behind the item")]
        public Sprite backdropImage;
        [Tooltip("The image that will display when the instance is empty")]
        public Sprite emptyItemImage;
        [Tooltip("The item in the slot")]
        public Item item;
        
        Image image;
        // Use this for initialization
        void Start() {
            Init();
            if (!backdropImage) {
                backdropImage = gameObject.GetComponent<Image>().sprite;
            }
            image = transform.GetChild(0).GetComponent<Image>();
            if (item) {
                image.sprite = item.icon;
                image.color = item.color;
            } else {
                image.sprite = emptyItemImage;
            }
        }
    }
}

namespace RPGSystem {
    public class iti : Draggable {
        public Sprite backdropImage;
        [Tooltip("The image that will display when the instance is empty")]
        public Sprite emptyItemImage;
        public Item item;

        // Use this for initialization
        void Start() {
            //backdropImage = gameObject.GetComponent<Image>().sprite;
            Init();
        }
    }
}