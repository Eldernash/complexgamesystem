﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPGSystem {
    public class ItemEntity : MonoBehaviour {
        [Tooltip("The item the character will pick up")]
        public Item item;

        private bool canPickup = false;
        // Update is called once per frame
        void Update() {
            if (!canPickup)
            canPickup = true;
        }

        private void OnTriggerEnter(Collider other) {
            Character character = other.GetComponent<Character>();
            if (character != null && canPickup) {
                for (int i = 0; i < character.inventory.Capacity; i++) {
                    if (character.inventory[i] == null) {
                        character.AddItem(item);
                        Destroy(this.gameObject);
                        break;
                    }
                }
            }
        }
    }
}
