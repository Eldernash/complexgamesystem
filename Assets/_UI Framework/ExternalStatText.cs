﻿using UnityEngine;
using UnityEngine.UI;

namespace RPGSystem {
    public class ExternalStatText : MonoBehaviour {

        public Character character;
        public Character.ExternalName affectedStat;
        private Text myText;

        private float currentValue;
        private float maxValue;
        // Use this for initialization
        void Start() {
            myText = gameObject.GetComponent<Text>();
            currentValue = character.GetExtStat(affectedStat).current;
            maxValue = character.GetExtStat(affectedStat).maximum;
        }

        // Update is called once per frame
        void Update() {
            currentValue = character.GetExtStat(affectedStat).current;
            maxValue = character.GetExtStat(affectedStat).maximum;
            myText.text = currentValue + "/" + maxValue;
        }
    }
}