﻿using UnityEngine;
using UnityEngine.UI;

namespace RPGSystem {

    public class ExternalStatCard : MonoBehaviour {

        public Character character;

        public Character.ExternalName affectedStat;

        private float currentValue;
        private float maxValue;
        private Image meter;

        // Use this for initialization
        void Start() {
            meter = gameObject.GetComponent<Image>();
            currentValue = character.GetExtStat(affectedStat).current;
            maxValue = character.GetExtStat(affectedStat).maximum;
        }

        // Update is called once per frame
        void Update() {
            currentValue = character.GetExtStat(affectedStat).current;
            maxValue = character.GetExtStat(affectedStat).maximum;
            meter.fillAmount = currentValue / maxValue;
        }
    }
}
