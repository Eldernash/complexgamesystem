﻿using UnityEngine;
using UnityEngine.UI;

namespace RPGSystem {
    public class UIField : MonoBehaviour {
        // makes all UI elements visible or invisible with the UI board.
        public void SetAllActive () {
            this.gameObject.SetActive(!this.enabled);
        }
    }
}