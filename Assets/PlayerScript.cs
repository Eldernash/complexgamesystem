﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour {

    public float speed;

    private CharacterController controller;

	// Use this for initialization
	void Start () {
        controller = gameObject.GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update () {
        float horzontal = 0;
        float vertical = 0;

        if (Input.GetKey(KeyCode.UpArrow)) { vertical += 1; }
        if (Input.GetKey(KeyCode.DownArrow)) { vertical -= 1; }
        if (Input.GetKey(KeyCode.LeftArrow)) { horzontal -= 1; }
        if (Input.GetKey(KeyCode.RightArrow)) { horzontal += 1; }

        controller.Move(new Vector3(horzontal, 0, vertical) * speed * Time.deltaTime);
    }
}
