﻿using UnityEngine;

namespace RPGSystem {

    public class Tester : MonoBehaviour {

        public Character testedCharacter;
        
        public float damage;

        public StatusEffect appliedBuff;
        public StatusEffect otherAppliedBuff;

        // Use this for initialization
        void Start() {

        }

        // Update is called once per frame
        void Update() {
            if (Input.GetKeyDown(KeyCode.A)) {
                testedCharacter.Damage(Character.ExternalName.HEALTH, damage);
            } else if (Input.GetKeyDown(KeyCode.S)) {
                appliedBuff.Apply(testedCharacter);
            } else if (Input.GetKeyDown(KeyCode.D)) {
                otherAppliedBuff.Apply(testedCharacter);
            }
}
    }
}
