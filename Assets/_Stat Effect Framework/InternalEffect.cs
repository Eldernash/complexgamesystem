﻿using UnityEngine;

namespace RPGSystem {

    [CreateAssetMenu(fileName = "New Internal Effect", menuName = "Status Effects/Internal Effect")]
    public class InternalEffect : StatusEffect {

        public enum EffectType {
            MODIFY
        };

        public EffectType effectType;

        public Character.InternalName affectedStat;

        override public void Tick() {

            // Increases/decreases the affected stat by the effect amount
            if (effectType == EffectType.MODIFY && !effectProcced) {
                affectedCharacter.ModifyIntMaximum(affectedStat, effectAmount);
                effectProcced = true;
            }
        }

        override public void RemoveEffect() {
            if (effectType == EffectType.MODIFY) {
                affectedCharacter.ModifyIntMaximum(affectedStat, -effectAmount);
            }
        }
    }
}
