﻿using UnityEngine;

namespace RPGSystem {

    public class StatusEffect : ScriptableObject {
        public new string name;

        public Character affectedCharacter;
        
        public float effectAmount = 0;

        public bool noLifeSpan = false;

        // Keep effect's lifespan above 0 or it will be considered "dead" and be removed
        public float effectLifeSpan = 1;

        [HideInInspector]
        public bool toRemove = false;

        [HideInInspector]
        public bool itemEffect = false;
        
        [HideInInspector]
        public bool effectProcced = false;

        private void Awake() {
            toRemove = false;
        }

        virtual public void Tick() {}

        public void Apply(Character character) {
            affectedCharacter = character;
            character.AddStatEffect(this);
        }
        
        virtual public void RemoveEffect() {}
    }
}
