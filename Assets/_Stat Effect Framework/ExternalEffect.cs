﻿using UnityEngine;

namespace RPGSystem {

    [CreateAssetMenu(fileName = "New External Effect", menuName = "Status Effects/External Effect")]
    public class ExternalEffect : StatusEffect {

        public enum EffectType {
            MODIFY = 0,
            OVERTIME
        }

        public EffectType effectType;

        public Character.ExternalName affectedStat;
        
        override public void Tick() {
            // Adds or subtracts the effect amount from the affected stat
            if (effectType == EffectType.OVERTIME) {
                affectedCharacter.ModifyExtStat(affectedStat, "current", effectAmount * Time.deltaTime);
            }

            // Increases/decreases the affected stat by the effect amount
            else if (effectType == EffectType.MODIFY && !effectProcced) {
                affectedCharacter.ModifyExtStat(affectedStat, "maximum", effectAmount);
                effectProcced = true;
            }
        }

        override public void RemoveEffect() {
            switch (effectType) {
                case EffectType.MODIFY:
                    affectedCharacter.ModifyExtStat(affectedStat, "maximum", -effectAmount);
                    break;
                case EffectType.OVERTIME:
                    effectAmount = 0;
                    break;
            }
        }
    }
}
