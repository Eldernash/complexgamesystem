﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace RPGSystem {
    public class Character : MonoBehaviour {

        public enum ExternalName {
            HEALTH,
            STAMINA,
            MANA
        };

        public enum InternalName {
            DAMAGE,
            DEFENCE
        }

        // the external stats of the characer (eg health, stamina)
        [System.Serializable]
        public struct ExternalStat {
            public ExternalName characterStat;
            public float current;
            public float maximum;
            public float baseMaximum;
        }

        // the internal stats of the character (eg strength, intelligence)
        [System.Serializable]
        public struct InternalStat {
            public InternalName characterStat;
            public float current;
        }
        // the internal stats of the character (eg strength, intelligence)
        [System.Serializable]
        public struct UIFields {
            public UIField field;
            public KeyCode toggleKey;
        }

        public ExternalStat[] m_externalStats;
        public InternalStat[] m_internalStats;

        public List<StatusEffect> m_statEffects;

        public Backpack backpack;

        public List<Item> inventory = new List<Item>();
        
        [Header("Equipped Items")]
        public Item helmet;
        public Item amulet;
        public Item ring1;
        public Item ring2;
        public Item gloves;
        public Item boots;
        public Item armour;

        [Header("UI Fields")]
        public List<UIFields> uiFields;

        // Update is called once per frame
        void Update() {

            // User controls
            for (int i = 0; i < uiFields.Count; i++) {
                if (Input.GetKeyDown(uiFields[i].toggleKey)){
                    uiFields[i].field.gameObject.SetActive(!uiFields[i].field.gameObject.activeSelf);
                }
            }

            List<StatusEffect> deathRow = new List<StatusEffect>();

            foreach (StatusEffect se in m_statEffects) {
                if (!se.noLifeSpan)
                    se.effectLifeSpan -= 1 * Time.deltaTime;
                if (se.effectLifeSpan <= 0 || se.toRemove) {
                    se.RemoveEffect();
                    deathRow.Add(se);
                }
                se.Tick();
            }
            foreach (StatusEffect se in deathRow)
                m_statEffects.Remove(se);

            // clamps the current stats to be between 0 and the maximum
            foreach (ExternalStat es in m_externalStats) {
                if (es.current > es.maximum) SetExtStat(es.characterStat, "current", es.maximum);
                else if (es.current < 0) SetExtStat(es.characterStat, "current", 0);
            }
        }

        public void Damage(ExternalName listIndex, float damage) {
            ExternalStat extTemp = GetExtStat(listIndex);

            extTemp.current = extTemp.current - damage;

            m_externalStats[(int)listIndex] = extTemp;
        }

        // INTERNAL STAT MANIPULATION

        // returns the external stat enum index by the name
        public ExternalStat GetExtStat(ExternalName name) {
            return m_externalStats[(int)name];
        }

        // sets the external stat to the decided amount
        public void SetExtStat(ExternalName name, string sName, float value) {
            switch (sName) {
                case "current":
                    m_externalStats[(int)name].current = value;
                    break;
                case "maximum":
                    m_externalStats[(int)name].maximum = value;
                    break;
                case "baseMaximum":
                    m_externalStats[(int)name].baseMaximum = value;
                    break;
                default:
                    break;
            }
        }
        // modifies the value of an external stat
        public void ModifyExtStat(ExternalName name, string sName, float delta) {
            switch (sName) {
                case "current":
                    m_externalStats[(int)name].current += delta;
                    break;
                case "maximum":
                    m_externalStats[(int)name].maximum += delta;
                    break;
                case "baseMaximum":
                    m_externalStats[(int)name].baseMaximum += delta;
                    break;
                default:
                    break;
            }
        }

        // INTERNAL STAT MANIPULATION

        // returns the internal stat enum index by the name
        public InternalStat GetIntStat(InternalName name) {
            return m_internalStats[(int)name];
        }
        // sets the internal stat to the decided amount
        public void SetIntCurrent(InternalName name, string sName, float value) {
            switch (sName) {
                case "current":
                    m_externalStats[(int)name].current = value;
                    break;
                case "maximum":
                    m_externalStats[(int)name].maximum = value;
                    break;
                default:
                    break;
            }
        }
        // modifies the value of an internal stat
        public void ModifyIntCurrent(InternalName name, string sName, float delta) {
            switch (sName) {
                case "current":
                    m_externalStats[(int)name].current += delta;
                    break;
                case "maximum":
                    m_externalStats[(int)name].maximum += delta;
                    break;
                default:
                    break;
            }
        }

        /// 
        /// DO NOT MODIFY BEYOND THIS POINT
        /// 

        // modifies the maximum value of an external stat
        public void ModifyIntMaximum(InternalName name, float delta) {
            m_internalStats[(int)name].current += delta;
        }

        // STATUS EFFECTS

        // Adds a copy of the status effect to the effects list
        public void AddStatEffect(StatusEffect statEff) {
            m_statEffects.Add(Instantiate(statEff));
        }
        // removes a status effect from the effects list
        public void RemoveStatEffect(StatusEffect statEff) {
            m_statEffects.Remove(statEff);
        }

        // recalculates all item status effects
        public void CheckItems() {
            List<StatusEffect> deathRow = new List<StatusEffect>();
            foreach (StatusEffect se in m_statEffects) {
                if (se != null && se.itemEffect) {
                    se.RemoveEffect();
                    deathRow.Add(se);
                }
            }

            foreach (StatusEffect se in deathRow)
                m_statEffects.Remove(se);

            CheckIsEquipped(helmet);
            CheckIsEquipped(amulet);
            CheckIsEquipped(ring1);
            CheckIsEquipped(ring2);
            CheckIsEquipped(gloves);
            CheckIsEquipped(boots);
            CheckIsEquipped(armour);
        }

        void CheckIsEquipped(Item item) {
            if (item != null) {
                item.Apply(this);
            }
        }

        // Add an item to the player inventory
        public void AddItem(Item item) {
            for (int i = 0; i < inventory.Count; i++) {
                if (inventory[i] == null) {
                    inventory[i] = (item);
                    break;
                }
            }
        }
    }
}
